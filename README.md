# Traffic visualization web-application
- Developing a web-application to animate probe-speed on the map, similar to google map, and to compare the traffic condition between the selected and historical time period. 
- The steps of implementing the web-appliction is on [Youtube](https://youtu.be/48YA1KVSWlI).
- The initilzed time takes 2-3 minutes for reading the entire csv files. 
- The end goal is to create a customized tool for traffic visualization and comparison.  

## Raw data description
NPMRDS transforms probe-based speed to segment speed. The segment information and speed is below,
- inrix_info.csv: Probe-based segment information
- inrix_speed.csv: segment speed and time between 2019-01-01 and 2019-11-31 on I-35 in Austin. 


### Reference
Raw data is provided by [NPMRDS](https://npmrds.ritis.org/analytics/). 